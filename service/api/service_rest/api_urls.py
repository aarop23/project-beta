from django.urls import path

from .views import (
    api_list_technicians,
    api_technicians,
    api_list_appointments,
    api_delete_appointment,

)

urlpatterns = [
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("technicians/<int:id>/", api_technicians, name="api_technicians"),
    path("appointments/", api_list_appointments, name="api_appointment"),
    path("appointments/<int:id>/", api_delete_appointment, name="api_delete_appointment")





]