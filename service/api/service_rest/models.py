from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True) 
    vin = models.CharField(max_length=17, unique=True)
    color = models.CharField(max_length=200, null=True)
    year = models.CharField(max_length=200, null=True)


    def __str__(self):
        return self.vin


class Technician(models.Model):
    employee_name = models.CharField(max_length=50, null=True)
    employee_id = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.employee_name
 

class Appointment(models.Model):
    name = models.CharField(max_length=50)
    time = models.DateTimeField(null=True)
    reservation = models.DateTimeField(null=True)
    vin = models.CharField(max_length=20, null=True)
    reason = models.TextField()

    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE,
    )

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="appointment",
        on_delete=models.CASCADE,
        null=True
    )

    def get_api_url(self):
        return reverse("api_list_service_appointments", kwargs={"pk": self.pk})


   






    
