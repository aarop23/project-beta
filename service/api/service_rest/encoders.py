from common.json import ModelEncoder

from .models import Technician, Appointment, AutomobileVO


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "employee_name",
        "employee_id",
        "id",
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "color",
        "year",
    ]

class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "name",
        "time",
        "reservation",
        "vin",
        "reason",
        "technician",
        "id",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
        "automobileid": AutomobileVOEncoder(),
    }
