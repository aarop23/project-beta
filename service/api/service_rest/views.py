from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Technician, Appointment
from .encoders import (
    TechnicianEncoder, AppointmentDetailEncoder
)


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentDetailEncoder
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            employee_id = content["technician"]
            technician = Technician.objects.get(id=employee_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician id"},
                status=400,
            )
        addtechnician = Appointment.objects.create(**content)
        return JsonResponse(
            addtechnician,
            encoder=AppointmentDetailEncoder,
            safe=False
        )


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        employee = Technician.objects.all()
        return JsonResponse(
            {"employee": employee},
            encoder=TechnicianEncoder,
        )
    else:
        content = json.loads(request.body)
        newemployee = Technician.objects.create(**content)
        return JsonResponse(
            newemployee,
            encoder=TechnicianEncoder,
            safe=False
        )

@require_http_methods(["DELETE"])
def api_technicians(request, id=None):
    if request.method == "DELETE":
        count, _ = Technician.objects.filter(id=id).delete()
        return JsonResponse({"deleted technician": count > 0})


@require_http_methods(["DELETE"])
def api_delete_appointment(request, id):
    if request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

