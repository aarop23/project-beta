import React, { useState, useEffect } from 'react';

function SaleForm() {
    const [automobiles, setAutomobiles] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [formData, setFormData] = useState({
        automobile_vin: '',
        salesperson_id: '',
        customer_id: '',
        price: '',
    });

    const getDataAutomobiles = async () => {
        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    };
    const getDataSalespeople = async () => {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salesperson);
        }
    };
    const getDataCustomers = async () => {
        const url = "http://localhost:8090/api/customers/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    };

    useEffect(() => {
        getDataAutomobiles();
        getDataSalespeople();
        getDataCustomers();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const locationUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                automobile_vin: '',
                salesperson_id: '',
                customer_id: '',
                price: '',
            });
        }
    }

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    const unsoldAutos = automobiles.filter(a => a.sold === false);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a new Sale</h1>
                    <form onSubmit={handleSubmit} id="create-sale-form">
                        <h4>Automobile VIN</h4>
                        <div className="form-floating mb-3">
                            <select required name="automobile_vin" onChange={handleFormChange} value={formData.automobile_vin} className="form-select">
                                <option disabled hidden value="">Choose an autmobile vin</option>
                                {unsoldAutos.map(auto => {
                                    return (
                                        <option key={auto.vin} value={auto.vin}>
                                            {auto.vin}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <h4>Salesperson</h4>
                        <div className="form-floating mb-3">
                            <select required name="salesperson_id" onChange={handleFormChange} value={formData.salesperson_id} className="form-select">
                                <option disabled hidden value="">Choose a salesperson</option>
                                {salespeople.map(salesperson => {
                                    return (
                                        <option key={salesperson.id} value={salesperson.id}>
                                            {salesperson.first_name} {salesperson.last_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <h4>Customer</h4>
                        <div className="form-floating mb-3">
                            <select required name="customer_id" onChange={handleFormChange} value={formData.customer_id} className="form-select">
                                <option disabled hidden value="">Choose a customer</option>
                                {customers.map(customer => {
                                    return (
                                        <option key={customer.id} value={customer.id}>
                                            {customer.first_name} {customer.last_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <h4>Price</h4>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="price" value={formData.price} required type="number" name="price" id="price" className="form-control" />
                            <label htmlFor="price">Price</label>
                        </div>
                        <div className="mb-3">
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SaleForm;
