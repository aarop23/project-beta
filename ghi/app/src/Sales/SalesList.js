import { useEffect, useState } from "react";

function SalesList() {
    const [sales, setSales] = useState([]);

    const FetchData = async () => {
        const response = await fetch("http://localhost:8090/api/sales/");
        if (response.ok) {
            const data = await response.json();
            setSales(data.salesrecord);
        }
    }

    useEffect(() => {
        FetchData();
    }, [])

    const handleDelete = async (event) => {
        const url = `http://localhost:8090/api/sales/${event.target.id}`;

        const fetchConfig = {
            method: 'delete',
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(url, fetchConfig);
        const data = await response.json();

    }



    return (
        <div className="row">
            <h1 className="text-left">Sales</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Employee ID</th>
                        <th>Salesperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(salesrecord => {
                        return (
                            <tr key={salesrecord.id}>
                                <td>{salesrecord.salesperson.id}</td>
                                <td>{salesrecord.salesperson.first_name} {salesrecord.salesperson.last_name}</td>
                                <td>{salesrecord.customer.first_name} {salesrecord.customer.last_name}</td>
                                <td>{salesrecord.automobile.vin}</td>
                                <td>{salesrecord.price}</td>
                                <td><button onClick={handleDelete} id={salesrecord.id} className="btn btn-sm btn-danger">Delete</button></td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>

    )
}

export default SalesList;
