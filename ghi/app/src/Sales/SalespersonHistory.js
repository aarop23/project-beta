import { useEffect, useState } from "react";

function SalesPersonHistory() {
    const [sales, setSales] = useState([]);
    const [filterList, setFilterList] = useState("");

    const fetchData = async () => {
        const response = await fetch("http://localhost:8090/api/sales/");
        if (response.ok) {
            const data = await response.json();
            setSales(data.salesrecord);
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    const filteredPeople = () => {
        return sales.filter((salesrecord) =>
            salesrecord.salesperson.first_name.toLowerCase().includes(filterList.toLowerCase()) ||
            salesrecord.salesperson.last_name.toLowerCase().includes(filterList.toLowerCase()) ||
            salesrecord.customer.first_name.toLowerCase().includes(filterList.toLowerCase()) ||
            salesrecord.customer.last_name.toLowerCase().includes(filterList.toLowerCase()) ||
            salesrecord.automobile.vin.toLowerCase().includes(filterList.toLowerCase()) ||
            salesrecord.price.includes(filterList)
        );
    };

    const handleFilterListChange = (e) => {
        const { value } = e.target;
        setFilterList(value);
    };



    return (
        <div className="row">
            <h1 className="text-left">Salesperson History</h1>
            <input
                onChange={handleFilterListChange}
                value={filterList}
                placeholder="Search for"
            />
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredPeople().map((salesrecord) => {
                        return (
                            <tr key={salesrecord.id}>
                                <td>{salesrecord.salesperson.first_name} {salesrecord.salesperson.last_name}</td>
                                <td>{salesrecord.customer.first_name} {salesrecord.customer.last_name}</td>
                                <td>{salesrecord.automobile.vin}</td>
                                <td>{salesrecord.price}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>

    )
}

export default SalesPersonHistory;
