import { useEffect, useState } from "react";


function SalePeopleList() {
    const [salespeople, setSalespeople] = useState([]);

    const getData = async () => {
        const response = await fetch("http://localhost:8090/api/salespeople/");
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salesperson);
        }
    }

    useEffect(() => {
        getData();
    }, [])

    const handleDelete = async (event) => {
        const url = `http://localhost:8090/api/salespeople/${event.target.id}`;

        const fetchConfig = {
            method: 'delete',
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(url, fetchConfig);
        const data = await response.json();

    }



    return (
        <div className="row">
            <h1 className="text-left">Salespeople</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map(salesperson => {
                        return (
                            <tr key={salesperson.id}>
                                <td>{salesperson.employee_id}</td>
                                <td>{salesperson.first_name}</td>
                                <td>{salesperson.last_name}</td>
                                <td><button onClick={handleDelete} id={salesperson.id} className="btn btn-danger">Delete</button></td>
                            </tr>
                        );

                    })}
                </tbody>
            </table>
        </div>
    )
}


export default SalePeopleList;
