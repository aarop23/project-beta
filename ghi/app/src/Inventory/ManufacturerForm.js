import React, { useState, useEffect } from 'react';

function ManufacturerForm() {
    const [formData, setFormData] = useState({
        name: '',
    })

    useEffect(() => {
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const locationUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                name: '',
            });
        }
    }

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create-customer-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Manufacturer name</label>
                        </div>
                        <div className="mb-3">
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ManufacturerForm;
