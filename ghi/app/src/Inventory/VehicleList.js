import { useEffect, useState } from "react";

function VehicleModelList() {
    const [vehicle, setVehicle] = useState([]);

    const fetchData = async () => {
        const response = await fetch("http://localhost:8100/api/models/");
        if (response.ok) {
            const data = await response.json();
            setVehicle(data.models);
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    const handleDelete = async (event) => {
        const url = `http://localhost:8100/api/models/${event.target.id}`;

        const fetchConfig = {
            method: 'delete',
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(url, fetchConfig);
        const data = await response.json();

    }



    return (
        <div className="row">
            <h1 className="text-left">Models</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Pictures</th>
                    </tr>
                </thead>
                <tbody>
                    {vehicle.map(models => {
                        return (
                            <tr key={models.id}>
                                <td>{models.name}</td>
                                <td>{models.manufacturer.name}</td>
                                <td><img src={models.picture_url} width="300" /></td>
                                <td><button onClick={handleDelete} id={models.id} className="btn btn-sm btn-danger">Delete</button></td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>

    )
}

export default VehicleModelList;
