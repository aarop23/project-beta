import { useEffect, useState } from "react";

function ManufacturersList() {
    const [manufacturers, setManufacturers] = useState([]);

    const getData = async () => {
        const response = await fetch("http://localhost:8100/api/manufacturers/");
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    useEffect(() => {
        getData();
    }, [])

    const handleDelete = async (event) => {
        const url = `http://localhost:8100/api/manufacturers/${event.target.id}`;

        const fetchConfig = {
            method: 'delete',
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(url, fetchConfig);
        const data = await response.json();

    }



    return (
        <div className="row">
            <h1 className="text-left">Manufacturers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manufacturers => {
                        return (
                            <tr key={manufacturers.id}>
                                <td>{manufacturers.name}</td>
                                <td><button onClick={handleDelete} id={manufacturers.id} className="btn btn-danger">Delete</button></td>
                            </tr>
                        );

                    })}
                </tbody>
            </table>
        </div>
    )
}


export default ManufacturersList;
