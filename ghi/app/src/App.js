import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalespeopleList from './Sales/SalespeopleList';
import SalesPersonForm from './Sales/salespeopleForm';
import CustomerForm from './Sales/CustomerForm';
import CustomerList from './Sales/CustomerList';
import SalesList from './Sales/SalesList';
import SaleForm from './Sales/SaleForm';
import SalespersonHistory from './Sales/SalespersonHistory'
import ManufacturersList from './Inventory/ManufacturersList';
import ManufacturerForm from './Inventory/ManufacturerForm';
import VehicleList from './Inventory/VehicleList';
import VehicleForm from './Inventory/VehicleForm';
import AppointmentForm from './Service/Appointmentform';
import TechnicianForm from './Service/Technicianform';
import Technicianlist from './Service/Technicianlist';
import Appointmentlist from './Service/Appointmentlist';
import CreateAutomobile from './Inventory/CreateAutomobiles';
import AutomobileList from './Inventory/ListAutomobiles';
import ServiceHistory from './Service/Servicehistory';
function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />}></Route>
          <Route path="manufacturers">
            <Route index element={<ManufacturersList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route index element={<VehicleList />} />
            <Route path="new" element={<VehicleForm />} />
          </Route>
          <Route path="salespeople">
            <Route index element={<SalespeopleList />} />
            <Route path="new" element={<SalesPersonForm />} />
          </Route>
          <Route path="customers">
            <Route index element={<CustomerList />} />
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="sales">
            <Route index element={<SalesList />} />
            <Route path="new" element={<SaleForm />} />
            <Route path="history" element={<SalespersonHistory />} />
          </Route>
          <Route path="appointments">
              <Route path="new" element={<AppointmentForm />} />
              <Route path="" element={<Appointmentlist />} />
              <Route path="history" element={<ServiceHistory />} />
          </Route>
          <Route path="technicians">
            <Route path="new" element={<TechnicianForm />} />
            <Route path="" element={<Technicianlist />} />
          </Route>
          <Route path="automobiles">
            <Route path="new" element={<CreateAutomobile />} />
            <Route path="" element={<AutomobileList />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
