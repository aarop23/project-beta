import React, { useState, useEffect } from "react";

function Technicianlist() {
    const [technician, setTechnicians] = useState([])
    const fetchData = async () => {
        const url = 'http://localhost:8081/api/technicians/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.employee);
            
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    const handleDelete = async (e) => {
        const url = `http://localhost:8081/api/technicians/${e.target.id}`

        const fetchConfigs = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }

        const response = await fetch(url, fetchConfigs)

        if (response.ok) {
            fetchData();
        } else {
            alert("Technician was not deleted");
        }
    };




    return (
    <>
        <div className="my-2 container" >
          <h1 className="py-2" style={{textAlign:'center'}}>List of Technicians</h1>
        </div>
        <hr/>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Employee Id</th>
                    <th>Technicians Name</th>
                 
                </tr>
            </thead>
            <tbody>
                {technician.map(employee => {
                    return (
                        <tr key={employee.id}>
                            <td>{employee.employee_id}</td>
                            <td>{employee.employee_name}</td>
                
                            <td><button onClick={handleDelete} id={employee.id} className="btn btn-danger">Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    </>
    );
}

export default Technicianlist;
