import React, { useEffect, useState } from 'react';


function Appointmentlist () {

    const [appointment, setAppointments] = useState([])
    const [vins, setVins] = useState([])

    const getData = async () => {

        const res = await fetch('http://localhost:8081/api/appointments/')
        if (res.ok) {
            const data = await res.json()
            setAppointments(data.appointments)
        }

        const response = await fetch('http://localhost:8100/api/automobiles/')
        if (response.ok) {
            const data = await response.json()
            setVins(data.autos)
        }
    }


    useEffect( () => {
        getData()
    }, [])


    const handleDelete = async (e) => {

        const appointmentUrl = `http://localhost:8081/api/appointments/${e.target.id}/`
        const fetchConfigs = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }

        const response = await fetch(appointmentUrl, fetchConfigs)
        getData()
    }


    return (
       <>
        <div className="my-2 container" >
          <h1 className="py-2" style={{textAlign:'center'}}>List of Automobiles</h1>
        </div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Customer name</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Purchased from Dealership</th>
                    <th> Action(Delete)</th>
                </tr>
            </thead>
            <tbody>
                {appointment.map(appointments => {
                   
                   const appointmenttime = new Date(appointments.time);
                   const appointmentTime = appointmenttime.toLocaleTimeString('en-US', { hour12: true });
                   const appointmentTimeString = appointmentTime.substring(0, appointmentTime.length - 6) + ' ' + appointmentTime.substring(appointmentTime.length - 2);
                   
                   const reservationDate = appointments.reservation.substring(0, 10);


                    let vip = "No"
                    for (let property of vins) {
                        if (appointments.vin === property.vin) {
                            vip = "Yes"
                        }
                    }

                return (
                    <tr key={appointments.id}>
                        <td>{ appointments.vin }</td>
                        <td>{ appointments.name }</td>
                        <td>{ reservationDate}</td>
                        <td>{ appointmentTimeString }</td>
                        <td>{ appointments.technician.employee_name }</td>
                        <td>{ appointments.reason }</td>
                        <td>{ vip }</td>
                        <td>
                            <div className="btn-group" role="group" aria-label="Basic example">
                                <button onClick={handleDelete} id={appointments.id} className="btn btn-danger">Cancel</button>         
                                <button onClick={handleDelete} id={appointments.id} className="btn btn-success">Finished</button>
                            </div>
                        </td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
       </>
    )
}
export default Appointmentlist
