import React, { useEffect, useState } from 'react';


function ServiceHistory () {

    const [appointment, setAppointments] = useState([])
    const [vin, setVin] = useState("")

    let vinFilter = ""

    const handleChange = (e) => {
        vinFilter = e.target.value
    }


    const getData = async () => {
        const res = await fetch('http://localhost:8081/api/appointments')

        if (res.ok) {
            const data = await res.json()
            setAppointments(data.appointments)
        }
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        setVin(vinFilter)
    }


    useEffect( () => {
        getData()
    }, [])

    
    return (
        <>
            <form>
                <table>
                    <tbody>
                        <tr>
                            <td><input style={{width:"60vw"}} onChange={handleChange} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control"></input>
                            <label htmlFor="vin"></label></td>
                            <td style={{ padding:0 }} className="form-control"><button onClick={handleSubmit}>Search</button></td>
                        </tr>
                    </tbody>
                </table>
            </form>
            <h1> ServiceHistory</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Finished</th>
                        <th>cancelled</th>

                    </tr>
                </thead>
                <tbody >
                    {appointment.map(appointments => {
                        
                        if (appointments.vin === vin) {
                            const appointmenttime = new Date(appointments.time);
                            const appointmentTime = appointmenttime.toLocaleTimeString('en-US', { hour12: true });
                            const appointmentTimeString = appointmentTime.substring(0, appointmentTime.length - 6) + ' ' + appointmentTime.substring(appointmentTime.length - 2);
                            
                            const reservationDate = appointments.reservation.substring(0, 10);
         
                    return (
                        <tr key={appointments.id}>
                        <td>{ appointments.vin }</td>
                        <td>{ appointments.name }</td>
                        <td>{ appointmentTimeString }</td>
                        <td>{ reservationDate }</td>
                        <td>{ appointments.technician.employee_name }</td>
                        <td>{ appointments.reason }</td>
                        <td>{ appointment.finished ? 'True' : 'False'}</td>
                        <td>{ appointment.cancelled ? 'True' : 'False' }</td>

                        </tr>
                    )};
                    })}
                </tbody>
            </table>
        </>
)}

export default ServiceHistory
