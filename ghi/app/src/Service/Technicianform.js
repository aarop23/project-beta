
import React, { useEffect, useState } from 'react';


function TechnicianForm () {

    const [formData, setFormData] = useState({
        employee_name: "",
        employee_id: "",
    })


    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value,
        })
    }
    const [submitStatus, setSubmitStatus] = useState(null);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const technicianUrl = 'http://localhost:8081/api/technicians/';
            const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
            };
            const response = await fetch(technicianUrl, fetchConfig);
            if (response.ok) {
                setFormData({
                    employee_name: "",
                    employee_id: "",
                })
                setSubmitStatus('success');
            }
        };


    return (
    
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Technician</h1>
            {submitStatus === 'success' && ( // Render the success message if the status is 'success'
                    <div className="alert alert-success" role="alert">
                        Technician added successfully!
                    </div>
                )}
            <form onSubmit={handleSubmit} id="create-technician-form">
                <div className="form-floating mb-3">
                    <input onChange={handleChange} value={formData.employee_name} placeholder="employee_name" required type="text" name="employee_name" id="employee_name" className="form-control"></input>
                    <label htmlFor="employee_name">Technician Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleChange} value={formData.employee_id} placeholder="Employee Id" required type="text" name="employee_id" id="employee_id" className="form-control"></input>
                    <label htmlFor="employee_id">Employee Number</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
    </div>
    )
}

export default TechnicianForm;
