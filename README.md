# CarCar

Team:

* Yin-Tung Chen - Sales microservice
* Aaron Carroll - service microservice

## Design
This project is designed to use Django to create RESTful APIs in microservices as back-end and use React to create a front-end application that uses the RESTful APIs that we built.

## Service microservice

I have defined a bounded context that includes both the Service API and Service Poller. After successfully setting up the Service Poller, I began creating models for Appointment, Technician, and Automobile. With these models in place, I created encoders to facilitate data transmission.

Next, I created views for the Service API, enabling users to list and create Appointments, Technicians, and Service history using GET and POST requests. After defining the relevant URLs in service_rest, I thoroughly tested the APIs to ensure their proper functioning.

With the backend complete, I have now moved on to the frontend development phase
Where I have developed React forms for the Appointment form, Appointment list, Service history, Technician list, and Technician create. Additionally, I have started work on inventory forms for creating and listing Automobiles.

## Sales microservice

In sales microservice involves four models; AutomobileVO (automobile value object), Salesperson, Customer, and Sale.
AutomobileVO using polling application to interact with an Automobile model inside of the Inventory microservice.
Salesperson model contains properties of a sales person, includes first name, last name, and the employee id to keep track individuals identities.
A customer model showing customer's first and last name, their's phone number and address. So it's easy to track customer's info.
In Sale model, it contains three foreign-key, basically track the sales record of automobile, salesperson, customer and sale prices.
