from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Salesperson, AutomobileVO, Customer, Sale
from . encoders import SalespersonEncoder, CustomerEncoder, SaleEncoder
from django.shortcuts import get_object_or_404
import json
import logging
import requests


@require_http_methods(["GET", "POST"])
def api_list_salesperson(request):
    if request.method == "GET":
        salesperson = Salesperson.objects.all()
        return JsonResponse(
            {"salesperson": salesperson},
            encoder=SalespersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create a sales person"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_detail_salesperson(request, id):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(id=id)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 400
            return response
    elif request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(id=id)
            salesperson.delete()
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.get(id=id)

            props = ["name"]
            for prop in props:
                if prop in content:
                    setattr(salesperson, prop, content[prop])
            salesperson.save()
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customers = Customer.objects.create(**content)
            return JsonResponse(
                customers,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not get customers"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def api_detail_customer(request, id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 400
            return response
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.get(id=id)

            props = ["name"]
            for prop in props:
                if prop in content:
                    setattr(customer, prop, content[prop])
            customer.save()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def api_list_salesrecord(request):
    if request.method == "GET":
        salesrecord = Sale.objects.all()
        return JsonResponse(
            {"salesrecord": salesrecord},
            encoder=SaleEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            # automobile foreign-key
            automobile_vin = content["automobile_vin"]
            automobile = get_object_or_404(AutomobileVO, vin=automobile_vin)
            if Sale.objects.filter(automobile=automobile).exists():
                return JsonResponse(
                    {"message": "The automobile has been sold!!"},
                    status=400,
                )
            # salesperson foreign-key
            salesperson_id = content["salesperson_id"]
            salesperson = get_object_or_404(Salesperson, pk=salesperson_id)
            # customer foreign-key
            customer_id = content["customer_id"]
            customer = get_object_or_404(Customer, pk=customer_id)
            salesrecord = Sale.objects.create(
                automobile=automobile,
                salesperson=salesperson,
                customer=customer,
                price=content["price"],
            )
            requests.put('http://project-beta-inventory-api-1:8000/api/automobiles/'+automobile_vin+'/', json={
                "sold": True
            })
            return JsonResponse(
                salesrecord,
                encoder=SaleEncoder,
                safe=False,
            )
        except Exception as e:
            logging.exception(e)
            response = JsonResponse(
                {"message": "Could not create a sales record"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_detail_salesrecord(request, id):
    if request.method == "GET":
        try:
            salesrecord = Sale.objects.get(id=id)
            return JsonResponse(
                salesrecord,
                encoder=SaleEncoder,
                safe=False
            )
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Sale record does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            salesrecord = Sale.objects.get(id=id)
            salesrecord.delete()
            return JsonResponse(
                salesrecord,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Sale record does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            salesrecord = Sale.objects.get(id=id)

            props = ["name"]
            for prop in props:
                if prop in content:
                    setattr(salesrecord, prop, content[prop])
            salesrecord.save()
            return JsonResponse(
                salesrecord,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
